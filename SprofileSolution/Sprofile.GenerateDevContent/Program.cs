﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bogus;
using SimpleInjector;
using Sprofile.Web.Common;
using Sprofile.Web.Infrastructure;
using Sprofile.Web.Model;

namespace Sprofile.GenerateDevContent
{
    class Bootstrap
    {
        public static Container container;

        public static void Start()
        {
            container = new Container();

            // Register your types, for instance:
            container.Register<ISprofileConfiguration, SprofileConfiguration>(Lifestyle.Singleton);
            container.Register<IElasticsearch, Elasticsearch>(Lifestyle.Singleton);
            container.Register<IActivityEventRepository, ActivityEventRepository>(Lifestyle.Singleton);
            container.Register<ILogRepository, LogRepository>(Lifestyle.Singleton);
            container.Register<ILogHelper, LogHelper>(Lifestyle.Singleton);

            // Optionally verify the container.
            container.Verify();
        }
    }


    class Program
    {
        private static readonly Random gen = new Random();
        private static DateTime GenerateDate()
        {
            DateTime start = new DateTime(2017, 4, 2, 16, 00, 00);
            int range = (DateTime.Today.AddDays(35) - start).Days;
            return start.AddDays(gen.Next(range));
        }

        static readonly Dictionary<int, List<string>> values = new Dictionary<int, List<string>>()
        {
            { 0, new List<string> { "Amerikans fotboll", "american-fotball.jpg" } },
            { 1, new List<string> { "Basket", "basketball.jpg" } },
            { 2, new List<string> { "Cykling", "cycling.jpg" } },
            { 3, new List<string> { "Fitness", "fitness.jpg" } },
            { 4, new List<string> { "Golf", "golf.jpg" } },
            { 5, new List<string> { "Kajakpaddling, Paddling", "kayaking.jpg" } },
            { 6, new List<string> { "Lacrosse", "lacrosse.jpg" } },
            { 7, new List<string> { "Maraton,Löpning", "marathon.jpg" } },
            { 8, new List<string> { "Moto GP", "moto.jpg" } },
            { 9, new List<string> { "Amerikans fotboll", "quarterback-67701.jpg" } },
            { 10, new List<string> { "Racing", "race.jpg" } },
            { 11, new List<string> { "Simning", "swimming.jpg" } },
        };

        
        private static readonly Random getrandom = new Random();
        private static readonly object syncLock = new object();
        public static int GetRandomNumber(int min, int max)
        {
            lock (syncLock)
            {
                return getrandom.Next(min, max);
            }
        }

        private static void GetValue(out string imageSrc, out string tag)
        {
            var rnd = GetRandomNumber(0, values.Count - 1);
            imageSrc = $"{values[rnd][1]}";
            tag = values[rnd][0];
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Start");

            Randomizer.Seed = new Random();
            var internet = new Bogus.DataSets.Internet(locale: "sv");
            var company = new Bogus.DataSets.Company(locale: "sv");
            var address = new Bogus.DataSets.Address(locale: "sv");

            Bootstrap.Start();

            var list = new List<ActivityEvent>();

            IActivityEventRepository _eventRepository = Bootstrap.container.GetInstance<IActivityEventRepository>();

            for (int i = 1; i <= 20; i++)
            {
                var start = GenerateDate();
                var end = start.AddHours(1);

                string imageSrc;
                string tag;
                GetValue(out imageSrc, out tag);

                //var i1 = i;
                var item = new Faker<ActivityEvent>()
                    .RuleFor(u => u.Header, f => f.Address.City() + " event")
                    .RuleFor(u => u.Description, f => string.Join(" ", f.Lorem.Words(7)))
                    .RuleFor(u => u.StartDate, f => start)
                    .RuleFor(u => u.EndDate, f => end)
                    .RuleFor(u => u.ImageUrl, f => imageSrc)
                    .RuleFor(u => u.Location, f => new Location() { City = address.City(), Address = address.StreetAddress(), Coordinates = new Coordinates() { Lat = 57.70179461, Lon = 11.97885704 } })
                    .RuleFor(u => u.Orginazer, f => new Organizer() { Email = internet.Email(), Name = company.CompanyName() })
                    .RuleFor(u => u.Tags, f => tag.Split(',').ToList())
                    ;

                var fake = item.Generate();
                list.Add(fake);
            }

            _eventRepository.InsertBulk(list);

            Console.WriteLine("Complete");
        }
    }
}
