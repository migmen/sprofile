﻿
namespace Sprofile.Web.Common
{
    public interface ISprofileConfiguration
    {
        string ElasticsearchUrl { get; }
        int ItemsPerPage { get; }
        int DaysInFuture { get; }
    }
}