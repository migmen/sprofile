﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;
using NLog;

namespace Sprofile.Web.Common
{
    public static class Logging
    {
        private static readonly Logger Logger = LogManager.GetLogger("SystemLogger");


        public static void Trace(string message, [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string memberName = "")
        {
            Logger.Trace(BuildMessage(message, LogType.Trace, filePath, lineNumber, memberName));
        }

        public static void Debug(string message, [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string memberName = "")
        {
            Logger.Debug(BuildMessage(message, LogType.Debug, filePath, lineNumber, memberName));
        }

        public static void Info(string message, [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string memberName = "")
        {
            Logger.Info(BuildMessage(message, LogType.Info, filePath, lineNumber, memberName));
        }

        public static void Warn(string message, [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string memberName = "")
        {
            Logger.Warn(BuildMessage(message, LogType.Warning, filePath, lineNumber, memberName));
        }

        public static void Error(string message, Exception exception = null, [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string memberName = "")
        {
            Logger.Error(exception, BuildMessage(message, LogType.Error, filePath, lineNumber, memberName));
        }

        public static string BuildMessage(string message, LogType logType, string filePath = "", int lineNumber = 0, string memberName = "")
        {
            var fileName = filePath;
            try
            {
                var indexOfLastSlash = filePath.LastIndexOf("\\");
                if (indexOfLastSlash != -1)
                    fileName = filePath.Substring(indexOfLastSlash + 1);
            }
            catch
            { }

            var threadId = Thread.CurrentThread?.ManagedThreadId ?? -1;
            var activityId = System.Diagnostics.Trace.CorrelationManager?.ActivityId.ToString() ?? string.Empty;

            var log = new LogEvent(message ?? "", activityId, threadId, fileName, lineNumber, memberName, logType);

            return Newtonsoft.Json.JsonConvert.SerializeObject(log);
        }
    }

    public enum LogType
    {
        Error,
        Warning,
        Info,
        Debug,
        Trace
    }

    public class LogEvent
    {
        public DateTime Occured => DateTime.Now;

        public string LogType { get; set; }

        public string Message { get; set; }
        public string ActivityId { get; set; }
        public int ThreadId { get; set; }

        public string Filename { get; set; }
        public int Line { get; set; }
        public string MethodName { get; set; }

        public LogEvent(string message, string activityId, int threadId, string filename, int line, string methodName, LogType logType)
        {
            LogType = logType.ToString();

            Message = message;
            ActivityId = activityId;
            ThreadId = threadId;

            Filename = filename;
            Line = line;
            MethodName = methodName;
        }
        public LogEvent()
        { }
    }
}
