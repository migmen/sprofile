﻿using System;
using System.Configuration;

namespace Sprofile.Web.Common
{
    public class SprofileConfiguration : ISprofileConfiguration
    {
        public string ElasticsearchUrl => ConfigurationManager.AppSettings["ElasticsearchUrl"];

        public int ItemsPerPage => ConfigurationManager.AppSettings["EventsPerPage"] == null ? 10 : Convert.ToInt32(ConfigurationManager.AppSettings["EventsPerPage"]);

        public int DaysInFuture => 7;
    }
}
