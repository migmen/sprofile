﻿using System.Web.Http;

namespace Sprofile.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            //config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new DefaultContractResolver();
            //config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new StringEnumConverter());

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);

            // Custom filters
            //config.Filters.Add(new Filter.ApiExceptionFilter(_logHelper));
        }
    }
}
