﻿using System;
using System.Collections.Generic;

namespace Sprofile.Web.Model
{
    // url: http://stackoverflow.com/a/14268167
    public class ActivityEvent : BaseModel
    {
        public ActivityEvent() : base()
        { }

        public string Header { get; set; }
        public string ImageUrl { get; set; }
        public Location Location { get; set; } = new Location();
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
        public Organizer Orginazer { get; set; } = new Organizer();
        public string Description { get; set; }
        public IEnumerable<string> Tags { get; set; }
    }

    // Postnummer, url: http://developer.bring.com/api/postal-code/
    // url: http://freegeoip.net
    // url: http://stackoverflow.com/questions/4327629/get-user-location-by-ip-address
    // url: https://github.com/remy/html5demos/blob/master/demos/geo.html
    // url: http://stackoverflow.com/questions/28068123/double-or-decimal-for-latitude-longitude-values-in-c-sharp
    public class Location
    {
        public string City { get; set; }
        public string Address { get; set; }
        public Coordinates Coordinates { get; set; }
    }

    public class Organizer
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string SiteUrl { get; set; }
        public string EventUrl { get; set; }
    }


    public class Coordinates
    {
        public double Lat { get; set; }
        public double Lon { get; set; }
    }
}