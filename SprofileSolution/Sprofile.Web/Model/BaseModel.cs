﻿using System;

namespace Sprofile.Web.Model
{
    public abstract class BaseModel
    {
        protected BaseModel()
        {
            Id = Guid.NewGuid();
            Occured = DateTimeOffset.Now;
        }

        public Guid Id { get; set; }
        public DateTimeOffset Occured { get; set; }
    }
}
