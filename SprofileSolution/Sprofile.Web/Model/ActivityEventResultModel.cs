﻿using System;
using System.Collections.Generic;

namespace Sprofile.Web.Model
{
    public class ActivityEventResultModel
    {
        public List<ActivityEvent> Events { get; set; }
        public long TotalHits { get; set; }
        public int ItemsPerPage { get; set; }
        public IEnumerable<Tag> Tags { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    public class Tag
    {
        public string Key { get; set; }
        public int Count { get; set; }
    }
}