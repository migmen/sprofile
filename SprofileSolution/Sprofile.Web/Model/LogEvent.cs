﻿
using System;

namespace Sprofile.Web.Model
{
    public class LogEvent : BaseModel
    {
        public LogEvent() : base()
        { }

        public string LogType { get; set; }
        public string Message { get; set; }
        public string FilePath { get; set; }
        public int LineNumber { get; set; }
        public string MemberName { get; set; }

        public string Exception { get; set; }
    }

    public class PerformanceMetrix : BaseModel
    {
        public PerformanceMetrix() : base()
        { }

        public string MessageType { get; set; }
        public string Method { get; set; }
        public string RequestUri { get; set; }
        public string AbsolutePath { get; set; }
        public string CorrelationId { get; set; }
        public string PathAndQuery { get; set; }
        public string Message { get; set; }
        public string DomainHost { get; set; }
        public string ActivityId { get; set; }
        public int ThreadId { get; set; }
        public int Bytes { get; set; }
        public double? ElapsedMilliseconds { get; set; }
        public double? ElapsedSeconds { get; set; }
        public string IpAddress { get; set; }
        public string Headers { get; set; }
        public string ContentType { get; set; }
        public string RouteTemplate { get; set; }
    }

    public enum MessageType
    {
        Request,
        Response
    }

    public class SearchLog : BaseModel
    {
        public SearchLog() : base()
        { }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Tags { get; set; }
        public string SearchString { get; set; }
        public int StartIndex { get; set; }
        public int Take { get; set; }
        public string Query { get; set; }
        public string Index { get; set; }
        public double? ElapsedMilliseconds { get; set; }
        public double? ElapsedSeconds { get; set; }
    }
}