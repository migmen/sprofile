﻿using System;
using System.Collections.Generic;

namespace Sprofile.Web.Model
{
    // url: http://json2csharp.com
    public class ActivityEventTagsGroup
    {
        public int took { get; set; }
        public bool timed_out { get; set; }
        public Shards _shards { get; set; }
        public Hits hits { get; set; }
        public Aggregations aggregations { get; set; }
    }

    public class Shards
    {
        public int total { get; set; }
        public int successful { get; set; }
        public int failed { get; set; }
    }

    public class Hits
    {
        public int total { get; set; }
        public decimal? max_score { get; set; }
        public List<Hit> hits { get; set; }
    }

    public class Hit
    {
        public string _index { get; set; } //": "sprofile-events",
        public string _type { get; set; } //": "activityevent",
        public Guid _id { get; set; } //": "6117b361-efa9-4f66-8670-bdf577f6bf32",
        public decimal? _score { get; set; } //": 5.937916,
        public ActivityEvent _source { get; set; }
    }

    public class Aggregations
    {
        public ByTags by_tags { get; set; }
    }

    public class ByTags
    {
        public int doc_count_error_upper_bound { get; set; }
        public int sum_other_doc_count { get; set; }
        public List<Bucket> buckets { get; set; }
    }

    public class Bucket
    {
        public string key { get; set; }
        public int doc_count { get; set; }
    }
}



/*
 {
  "took": 4,
  "timed_out": false,
  "_shards": {
    "total": 5,
    "successful": 5,
    "failed": 0
  },
  "hits": {
    "total": 2,
    "max_score": 6.7120557,
    "hits": [
      {
        "_index": "sprofile-events",
        "_type": "activityevent",
        "_id": "ea376916-f59c-4d2c-83e7-bb3beec1bf5b",
        "_score": 6.7120557,
        "_source": {
          "header": "Port Hughshire event",
          "imageUrl": "images/stock/golf.jpg",
          "location": {
            "city": "Östersta",
            "address": "Östra Nyallén 4",
            "latitude": 0,
            "longitude": 0
          },
          "startDate": "2017-03-19T16:00:00+01:00",
          "endDate": "2017-03-19T17:00:00+01:00",
          "orginazer": {
            "name": "Karlsson, Karlsson and Johansson",
            "email": "Alan_Gustafsson@yahoo.com"
          },
          "description": "voluptatem dolore ut vitae eius sed quo",
          "tags": [
            "Golf"
          ],
          "id": "ea376916-f59c-4d2c-83e7-bb3beec1bf5b",
          "occured": "2017-02-28T21:28:59.8568632+01:00"
        }
      },
      {
        "_index": "sprofile-events",
        "_type": "activityevent",
        "_id": "6117b361-efa9-4f66-8670-bdf577f6bf32",
        "_score": 5.937916,
        "_source": {
          "header": "Port Allenton event",
          "imageUrl": "images/stock/golf.jpg",
          "location": {
            "city": "Kristby",
            "address": "Nilssons Gata 2",
            "latitude": 0,
            "longitude": 0
          },
          "startDate": "2017-03-16T16:00:00+01:00",
          "endDate": "2017-03-16T17:00:00+01:00",
          "orginazer": {
            "name": "Andersson Investment",
            "email": "Bradly_Eriksson16@gmail.com"
          },
          "description": "repudiandae culpa aut quam cum accusamus possimus",
          "tags": [
            "Golf"
          ],
          "id": "6117b361-efa9-4f66-8670-bdf577f6bf32",
          "occured": "2017-02-28T21:28:59.7631454+01:00"
        }
      }
    ]
  },
  "aggregations": {
    "by_tags": {
      "doc_count_error_upper_bound": 0,
      "sum_other_doc_count": 0,
      "buckets": [
        {
          "key": "Golf",
          "doc_count": 2
        }
      ]
    }
  }
}
*/
