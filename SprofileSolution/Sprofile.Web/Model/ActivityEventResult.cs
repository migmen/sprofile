﻿using System.Collections.Generic;

namespace Sprofile.Web.Model
{
    public class ActivityEventResult
    {
        public IEnumerable<ActivityEvent> Events { get; set; }
        public long TotalHits { get; set; }
        public int ItemsPerPage { get; set; }
    }
}