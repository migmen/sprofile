﻿using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using SimpleInjector.Integration.WebApi;
using Sprofile.Web.Infrastructure;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using Sprofile.Web.Common;

namespace Sprofile.Web.Filter
{
    public static class SimpleInjectorWebApiInitializer
    {
        /// <summary>Initialize the container and register it as MVC Dependency Resolver.</summary>
        public static void Initialize()
        {
            var container = new Container();
            //container.Options.DefaultScopedLifestyle = new WebApiRequestLifestyle();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            InitializeContainer(container);

            // This is an extension method from the integration package.
            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            // This is an extension method from the integration package as well.
            container.RegisterMvcIntegratedFilterProvider();


            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);

            container.Verify();


            // MVC
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));

            // WebApi
            GlobalConfiguration.Configuration.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);

            // Fix for ExceptionFilterAttribute
            var _logHelper = (ILogHelper)GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(ILogHelper));
            GlobalConfiguration.Configuration.Filters.Add(new ApiExceptionFilter(_logHelper));
        }

        internal static void InitializeContainer(Container container)
        {
            // Register your types, for instance:
            container.Register<ISprofileConfiguration, SprofileConfiguration>(Lifestyle.Scoped);
            container.Register<IElasticsearch, Infrastructure.Elasticsearch>(Lifestyle.Scoped);
            container.Register<IActivityEventRepository, ActivityEventRepository>(Lifestyle.Scoped);
            container.Register<ILogRepository, LogRepository>(Lifestyle.Scoped);
            container.Register<ILogHelper, LogHelper>(Lifestyle.Scoped);
        }
    }
}