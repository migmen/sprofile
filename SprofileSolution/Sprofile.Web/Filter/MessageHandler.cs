﻿using Sprofile.Web.Infrastructure;
using System;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Sprofile.Web.Filter
{
    public abstract class MessageHandler : DelegatingHandler
    {
        private ILogHelper _logHelper;
        protected ILogHelper Log()
        {
            if (_logHelper == null)
            {
                _logHelper = (ILogHelper)GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(ILogHelper));
            }
            return _logHelper;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var corrId = string.Format("{0}{1}", DateTime.Now.Ticks, Thread.CurrentThread.ManagedThreadId);
            //var requestInfo = string.Format("{0} {1}", request.Method, request.RequestUri);

            var requestMessage = await request.Content.ReadAsByteArrayAsync();

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            await IncommingMessageAsync(corrId, request, requestMessage, Log());

            var response = await base.SendAsync(request, cancellationToken);

            byte[] responseMessage = null;

            if (response.IsSuccessStatusCode)
            {
                if (response.Content != null)
                {
                    responseMessage = await response.Content.ReadAsByteArrayAsync().ContinueWith((task) => task.Result);
                }
            }
            else
            {
                responseMessage = Encoding.UTF8.GetBytes(response.ReasonPhrase);
            }

            stopwatch.Stop();
            await OutgoingMessageAsync(corrId, request, responseMessage, stopwatch, Log());

            return response;
        }


        protected abstract Task IncommingMessageAsync(string correlationId, HttpRequestMessage request, byte[] message, ILogHelper log);
        protected abstract Task OutgoingMessageAsync(string correlationId, HttpRequestMessage request, byte[] message, Stopwatch stopwatch, ILogHelper log);
    }


    public class MessageLoggingHandler : MessageHandler
    {
        protected override async Task IncommingMessageAsync(string correlationId, HttpRequestMessage request, byte[] message, ILogHelper log)
        {
            //int length = message.Length;
            //var msg = Encoding.UTF8.GetString(message);

            //await Task.Run(() => Logging.Trace($"{correlationId} - Request: {requestInfo}, {length} bytes{string.Empty}, Message:{msg}."));
            await Task.Run(() =>
                log.PerformanceMessage(Model.MessageType.Request, correlationId, request, message)
                //log.Trace($"{correlationId} - Request: {requestInfo}, {length} bytes{string.Empty}, Message:{msg}.")
                );
        }


        protected override async Task OutgoingMessageAsync(string correlationId, HttpRequestMessage request, byte[] message, Stopwatch stopwatch, ILogHelper log)
        {
            //int length = message.Length;
            //var msg = Encoding.UTF8.GetString(message);

            //await Task.Run(() => Logging.Trace($"{correlationId} - Response: {requestInfo}, {length} bytes, Elapsed:{stopWatch.Elapsed}."));
            await Task.Run(() =>
                log.PerformanceMessage(Model.MessageType.Response, correlationId, request, message, stopwatch)
                //log.Trace($"{correlationId} - Response: {requestInfo}, {length} bytes, Elapsed:{stopWatch.Elapsed}.")
                );
        }
    }


    //public static class IsImageHelper
    //{
    //    public static bool IsImageAndFormat(byte[] contentBytes)
    //    {
    //        bool isImage = false;

    //        if (ImageFormat.Jpeg.Equals(contentBytes))
    //        {
    //            isImage = true;
    //            // JPEG
    //        }
    //        else if (ImageFormat.Png.Equals(contentBytes))
    //        {
    //            isImage = true;
    //            // PNG
    //        }
    //        else if (ImageFormat.Gif.Equals(contentBytes))
    //        {
    //            isImage = true;
    //            // GIF
    //        }

    //        return isImage;
    //    }
    //}
}