﻿using Sprofile.Web.Infrastructure;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Filters;

namespace Sprofile.Web.Filter
{
    public class ApiExceptionFilter : ExceptionFilterAttribute
    {
        private ILogHelper _logHelper;
        public ApiExceptionFilter(ILogHelper logHelper)
        {
            _logHelper = logHelper;
        }

        public override void OnException(HttpActionExecutedContext ctx)
        {
            // Base error filter
            var httpResponseException = new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
                // "An error occurred, please try again or contact the administrator."
                Content = new StringContent("Ett fel har inträffat, var god försök igen eller kontakta supporten."),
                ReasonPhrase = "Error Exception"
            });

            //var ex = ctx.Exception as HttpResponseException;

            string requestedUri = null;
            HttpRequestMessage request = ctx.Request;
            if (request?.RequestUri != null)
            {
                requestedUri = request.RequestUri.ToString();
            }

            _logHelper.Error($"ApiError RequestUri:{requestedUri}, {ctx.Exception.Message}.", ctx.Exception);

            throw httpResponseException;
        }
    }
}