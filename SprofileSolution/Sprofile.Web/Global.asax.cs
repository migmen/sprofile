﻿using System;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Sprofile.Web.Infrastructure;
using Sprofile.Web.Filter;

namespace Sprofile.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            GlobalConfiguration.Configuration.MessageHandlers.Add(new MessageLoggingHandler());

            SimpleInjectorWebApiInitializer.Initialize();
        }


        protected void Application_Error(Object sender, EventArgs e)
        {
            var exception = Server.GetLastError();
            if (exception == null)
                return;

            var _logHelper = (ILogHelper)GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(ILogHelper));
            _logHelper.Error($"Site Application_Error: {exception.Message}.", exception);

            // Clear the error
            Server.ClearError();

            Response.Redirect($"home/error?message={exception.Message}");
        }
    }
}
