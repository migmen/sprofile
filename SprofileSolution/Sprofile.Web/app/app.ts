﻿/// <reference path="../node_modules/@types/jquery/index.d.ts" />
/// <reference path="../node_modules/@types/jquery.timeago/index.d.ts" />

class Sprofile {
    _pageIndex: number = 1;
    _template: string = "<div class=\"card\" data-id=\"{id}\">\
            <div class=\"image\">\
                <img src=\"http://res.cloudinary.com/forloop/image/upload/c_thumb,h_180,w_250/sprofile/{imgsrc}\">\
            </div>\
            <div class=\"content\">\
                <div class=\"header\">{header}</div>\
                <div class=\"meta\">\
                    <a class=\"ui small label\"><i class=\"calendar icon\"></i><time class=\"timeago\" datetime=\"{startdatetime}\" title=\"{startdate}\">-</time></a>\
                    <div class=\"ui small label\"><i class=\"wait icon\"></i> {starttime} - {endtime}</div>\
                </div>\
                <div class=\"description\">\
                    <p>{description}</p>\
                    <p class=\"mini\">Arrangör: {name} ({email})</p>\
                </div>\
                <div>{tags}</div>\
            </div>\
            <div class=\"extra content\">\
                <i class=\"marker icon\"></i>{city}, {address}\
            </div>\
            </div>";
    _tagTemplate: string = "<a class=\"ui label\" data-type=\"tag\" data-tag=\"{key}\">{key}<div class=\"detail\">{count}</div></a>";
    _modalTemplate: string = "<div class=\"header\">{header}</div>\
    <div class=\"content\">\
        <div class=\"ui image\">\
            <img src=\"http://res.cloudinary.com/forloop/image/upload/c_thumb,h_200,w_260/sprofile/{imgsrc}\">\
        </div>\
        <div class=\"content\">\
            <div class=\"meta\">\
                <a class=\"ui small label\"><i class=\"calendar icon\"></i><time class=\"timeago\" datetime=\"{startdatetime}\" title=\"{startdate}\">-</time></a>\
                <div class=\"ui small label\"><i class=\"wait icon\"></i> {starttime} - {endtime}</div>\
            </div>\
            <div class=\"description\">\
                <p>{description}</p>\
                <p class=\"mini\">Arrangör: {name} ({email})</p>\
            </div>\
            <div class=\"extra content\">\
                <i class=\"marker icon\"></i>{city}, {address}\
            </div>\
        </div>\
    </div>";
    _tags: string[] = [];
    _daysInFuture: number = 7;
    _lastTagObj: any = null;

    constructor() {
    }

    getAjax = (url: string, success: any) => {
        $.ajax({
            method: "GET",
            url: url,
            dataType: "json",
        })
        .done(success);
    }

    formatDate = (d: any) => {
        let month = d.getMonth() + 1;
        let day = d.getDate();

        let output = d.getFullYear() + '-' +
            (('' + month).length < 2 ? '0' : '') + month + '-' +
            (('' + day).length < 2 ? '0' : '') + day;
        return output;
    }

    getDate = () => {
        return this.formatDate(new Date());
    }

    addZero = (num: any) => {
        if (String(num).length === 1) { return "0" + num; }
        return num;
    }

    m = (month: number) => {
        return month + 1;
    }

    itemContent = (evt: any) => {
        let sd = new Date(evt.StartDate);
        let ed = new Date(evt.EndDate);
        let event = this._template
            .replace("{imgsrc}", evt.ImageUrl)
            .replace("{header}", evt.Header)
            .replace("{startdatetime}", sd.getFullYear() + "-" + this.m(sd.getMonth()) + "-" + sd.getDate() + " " + this.addZero(sd.getHours()) + ":" + this.addZero(sd.getMinutes()))
            .replace("{startdate}", sd.getFullYear() + "-" + this.m(sd.getMonth()) + "-" + sd.getDate())
            .replace("{starttime}", this.addZero(sd.getHours()) + ":" + this.addZero(sd.getMinutes()))
            .replace("{endtime}", this.addZero(ed.getHours()) + ":" + this.addZero(ed.getMinutes()))
            .replace("{description}", evt.Description)
            .replace("{name}", evt.Orginazer.Name)
            .replace("{email}", evt.Orginazer.Email)
            .replace("{city}", evt.Location.City)
            .replace("{address}", evt.Location.Address)
            .replace("{tags}", evt.Tags.join(", "))
            .replace("{id}", evt.Id)
            ;
        $("#eventcards").append(event);
    }

    tagItemContent = (t: any) => {
        let tag = this._tagTemplate
            .replace(/{key}/g, t.Key)
            .replace("{count}", t.Count);

        $("#tags").append(tag);
    }

    modalContent = (i: any) => {
        let sd = new Date(i.StartDate);
        let ed = new Date(i.EndDate);
        let modal = this._modalTemplate
            .replace("{imgsrc}", i.ImageUrl)
            .replace("{header}", i.Header)
            .replace("{startdatetime}", sd.getFullYear() + "-" + this.m(sd.getMonth()) + "-" + sd.getDate() + " " + this.addZero(sd.getHours()) + ":" + this.addZero(sd.getMinutes()))
            .replace("{startdate}", sd.getFullYear() + "-" + this.m(sd.getMonth()) + "-" + sd.getDate())
            .replace("{starttime}", this.addZero(sd.getHours()) + ":" + this.addZero(sd.getMinutes()))
            .replace("{endtime}", this.addZero(ed.getHours()) + ":" + this.addZero(ed.getMinutes()))
            .replace("{description}", i.Description)
            .replace("{name}", i.Orginazer.Name)
            .replace("{email}", i.Orginazer.Email)
            .replace("{city}", i.Location.City)
            .replace("{address}", i.Location.Address)
            .replace("{tags}", i.Tags.join(", "))
            .replace("{id}", i.Id)
            ;

        $("#modal").empty();
        $("#modal").append(modal);
    }

    moreButton = (total: number, perPage: number) => {
        let loaded = (this._pageIndex + 1) * perPage;
        if (total <= loaded) {
            $("#more").hide();
        }
        else {
            $("#more").show();
        }
    }

    successLoadingEvents = (response: any) => {
        let events = response.Events;
        let itemsProcessed = 0;
        events.forEach((event: any, index: any, array: any) => {
            this.itemContent(event);
            itemsProcessed++;
            if (itemsProcessed === array.length) {
                $("time.timeago").timeago();
            }
        });
        this.moreButton(response.TotalHits, response.ItemsPerPage);
        this._pageIndex++;

        // Tags
        let tags = response.Tags;
        itemsProcessed = 0;
        tags.forEach((tg: any, index: any, array: any) => {
            this.tagItemContent(tg);
            itemsProcessed++;
            if (itemsProcessed === array.length) {
                $("a[data-type='tag']").click(this.filterTag);
                this.markTag();
            }
        });
        
    }

    emptyElements = () => {
        $("#eventcards").empty();
        $("#tags").empty();
    }

    search = () => {
        this.emptyElements();
        this._pageIndex = 0;

        this.executeSearch();
    }

    markTag = () => {
        if (this._tags.length > 0) {
            this._tags.forEach((tg: any, index: any, array: any) => {
                var $callback_this = $("a[data-tag='" + tg + "']")
                $callback_this.addClass("blue");
                $callback_this.append("<i class=\"delete icon\"></i>");
                $callback_this.unbind("click");
            });
        }
    }

    filterTag = (e: any) => {
        this._lastTagObj = $(e.target);
        if (this._lastTagObj.hasClass("detail")) {
            this._lastTagObj = this._lastTagObj.parent();
        }
        this._tags.push(this._lastTagObj.data("tag"));

        this.search();
    }

    executeSearch = () => {
        let searchString: string = $("#search-input").val();
        let url = "api/events/filter?d=" + this._daysInFuture + "&i=" + this._pageIndex + "&t=" + this._tags.join(",") + "&f=" + searchString;
        this.getAjax(url, this.successLoadingEvents);
    }

    removeTag = (e: any) => {
        var $tagToRemove = $(e.target);
        if ($tagToRemove.hasClass("delete")) {
            $tagToRemove = $tagToRemove.parent();
        }

        this._tags = this._tags.filter(function (el) {
            return el !== $tagToRemove.data("tag");
        });

        this.search();
    }

    updateDays = (e: any) => {
        $("#tags").empty();

        var $day = $(e.target);
        this._daysInFuture = $day.data("days");

        $("a[data-type='days']").removeClass("blue");
        $day.addClass("blue");

        this.search();
    }

    more = () => {
        $("#tags").empty();
        this.executeSearch();
    }

    detail = (e: any) => {
        var $card = $(e.currentTarget);
        var id = $card.data("id");

        console.log("modal: " + id);

        this.getAjax("api/events/" + id, (response: any) => {
            this.modalContent(response);
            $("time.timeago").timeago();
            (<any>$('.ui.modal')).modal('setting', 'transition', 'fade up').modal('show');
        });
    }

    public start = () => {
        $.timeago.settings.allowFuture = true;
        $("time.timeago").timeago();

        $("#more").click(this.more);
        $("#search-btn").click(this.search);
        $('#search-input').keypress(e => {
            if (e.which === 13) {
                this.search();
            }
        });

        $("a[data-type='tag']").click(this.filterTag);
        $(document).on("click", "i[class='delete icon']", this.removeTag);
        $("a[data-type='days']").click(this.updateDays);
        $(document).on("click", "div[class='card']", this.detail);
    }



    successImages = (response: any) => {
        var list = $('#img-list');
        $.each(response, function (key, value) {
            list.append($("<option></option>")
                .attr("value", key)
                .text(value));
        });

        list.change(() => {
            var file = $("#img-list option:selected").text();

            $('#img-preview').empty();
            $('#img-preview').prepend("<img src=\"http://res.cloudinary.com/forloop/image/upload/c_scale,h_250/sprofile/" + file + "\" />");
        });
    }

    getImages = () => {
        this.getAjax("/api/events/images", this.successImages);
    }

    createEvent = () => {
        var formData = $('form').serializeArray().reduce((obj: any, item: any) => {
            obj[item.name] = item.value;
            return obj;
        }, {});

        var eventJson = {
            "apiKey": formData["api[key]"],
            "header": formData["event[header]"],
            "description": formData["event[description]"],
            "startDate": formData["event[startdate]"],
            "endDate": formData["event[enddate]"],
            "imageUrl": $("#img-list option:selected").text(),
            "tags": formData["event[tags]"].split(','),
            "location": {
                "address": formData["location[address]"],
                "city": formData["location[city]"],
                "latitude": formData["location[latitude]"],
                "longitude": formData["location[longitude]"],
            },
            "orginazer": {
                "email": formData["orginazer[email]"],
                "name": formData["orginazer[name]"],
                "phone": formData["orginazer[phone]"],
                "siteUrl": formData["orginazer[siteUrl]"],
                "eventUrl": formData["orginazer[eventUrl]"],
            }
        };

        $.ajax({
            method: "POST",
            url: "/api/events",
            
            data: eventJson,
            dataType: "json",
        })
        .done((response) => alert("Done! " + response))
        .fail((callback) => alert("Error: " + callback));
    }

    public create = () => {
        $("#create-btn").click(this.createEvent);

        this.getImages();
    }
}