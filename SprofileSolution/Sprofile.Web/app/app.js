/// <reference path="../node_modules/@types/jquery/index.d.ts" />
/// <reference path="../node_modules/@types/jquery.timeago/index.d.ts" />
var Sprofile = (function () {
    function Sprofile() {
        var _this = this;
        this._pageIndex = 1;
        this._template = "<div class=\"card\" data-id=\"{id}\">\
            <div class=\"image\">\
                <img src=\"http://res.cloudinary.com/forloop/image/upload/c_thumb,h_180,w_250/sprofile/{imgsrc}\">\
            </div>\
            <div class=\"content\">\
                <div class=\"header\">{header}</div>\
                <div class=\"meta\">\
                    <a class=\"ui small label\"><i class=\"calendar icon\"></i><time class=\"timeago\" datetime=\"{startdatetime}\" title=\"{startdate}\">-</time></a>\
                    <div class=\"ui small label\"><i class=\"wait icon\"></i> {starttime} - {endtime}</div>\
                </div>\
                <div class=\"description\">\
                    <p>{description}</p>\
                    <p class=\"mini\">Arrangör: {name} ({email})</p>\
                </div>\
                <div>{tags}</div>\
            </div>\
            <div class=\"extra content\">\
                <i class=\"marker icon\"></i>{city}, {address}\
            </div>\
            </div>";
        this._tagTemplate = "<a class=\"ui label\" data-type=\"tag\" data-tag=\"{key}\">{key}<div class=\"detail\">{count}</div></a>";
        this._modalTemplate = "<div class=\"header\">{header}</div>\
    <div class=\"content\">\
        <div class=\"ui image\">\
            <img src=\"http://res.cloudinary.com/forloop/image/upload/c_thumb,h_200,w_260/sprofile/{imgsrc}\">\
        </div>\
        <div class=\"content\">\
            <div class=\"meta\">\
                <a class=\"ui small label\"><i class=\"calendar icon\"></i><time class=\"timeago\" datetime=\"{startdatetime}\" title=\"{startdate}\">-</time></a>\
                <div class=\"ui small label\"><i class=\"wait icon\"></i> {starttime} - {endtime}</div>\
            </div>\
            <div class=\"description\">\
                <p>{description}</p>\
                <p class=\"mini\">Arrangör: {name} ({email})</p>\
            </div>\
            <div class=\"extra content\">\
                <i class=\"marker icon\"></i>{city}, {address}\
            </div>\
        </div>\
    </div>";
        this._tags = [];
        this._daysInFuture = 7;
        this._lastTagObj = null;
        this.getAjax = function (url, success) {
            $.ajax({
                method: "GET",
                url: url,
                dataType: "json",
            })
                .done(success);
        };
        this.formatDate = function (d) {
            var month = d.getMonth() + 1;
            var day = d.getDate();
            var output = d.getFullYear() + '-' +
                (('' + month).length < 2 ? '0' : '') + month + '-' +
                (('' + day).length < 2 ? '0' : '') + day;
            return output;
        };
        this.getDate = function () {
            return _this.formatDate(new Date());
        };
        this.addZero = function (num) {
            if (String(num).length === 1) {
                return "0" + num;
            }
            return num;
        };
        this.m = function (month) {
            return month + 1;
        };
        this.itemContent = function (evt) {
            var sd = new Date(evt.StartDate);
            var ed = new Date(evt.EndDate);
            var event = _this._template
                .replace("{imgsrc}", evt.ImageUrl)
                .replace("{header}", evt.Header)
                .replace("{startdatetime}", sd.getFullYear() + "-" + _this.m(sd.getMonth()) + "-" + sd.getDate() + " " + _this.addZero(sd.getHours()) + ":" + _this.addZero(sd.getMinutes()))
                .replace("{startdate}", sd.getFullYear() + "-" + _this.m(sd.getMonth()) + "-" + sd.getDate())
                .replace("{starttime}", _this.addZero(sd.getHours()) + ":" + _this.addZero(sd.getMinutes()))
                .replace("{endtime}", _this.addZero(ed.getHours()) + ":" + _this.addZero(ed.getMinutes()))
                .replace("{description}", evt.Description)
                .replace("{name}", evt.Orginazer.Name)
                .replace("{email}", evt.Orginazer.Email)
                .replace("{city}", evt.Location.City)
                .replace("{address}", evt.Location.Address)
                .replace("{tags}", evt.Tags.join(", "))
                .replace("{id}", evt.Id);
            $("#eventcards").append(event);
        };
        this.tagItemContent = function (t) {
            var tag = _this._tagTemplate
                .replace(/{key}/g, t.Key)
                .replace("{count}", t.Count);
            $("#tags").append(tag);
        };
        this.modalContent = function (i) {
            var sd = new Date(i.StartDate);
            var ed = new Date(i.EndDate);
            var modal = _this._modalTemplate
                .replace("{imgsrc}", i.ImageUrl)
                .replace("{header}", i.Header)
                .replace("{startdatetime}", sd.getFullYear() + "-" + _this.m(sd.getMonth()) + "-" + sd.getDate() + " " + _this.addZero(sd.getHours()) + ":" + _this.addZero(sd.getMinutes()))
                .replace("{startdate}", sd.getFullYear() + "-" + _this.m(sd.getMonth()) + "-" + sd.getDate())
                .replace("{starttime}", _this.addZero(sd.getHours()) + ":" + _this.addZero(sd.getMinutes()))
                .replace("{endtime}", _this.addZero(ed.getHours()) + ":" + _this.addZero(ed.getMinutes()))
                .replace("{description}", i.Description)
                .replace("{name}", i.Orginazer.Name)
                .replace("{email}", i.Orginazer.Email)
                .replace("{city}", i.Location.City)
                .replace("{address}", i.Location.Address)
                .replace("{tags}", i.Tags.join(", "))
                .replace("{id}", i.Id);
            $("#modal").empty();
            $("#modal").append(modal);
        };
        this.moreButton = function (total, perPage) {
            var loaded = (_this._pageIndex + 1) * perPage;
            if (total <= loaded) {
                $("#more").hide();
            }
            else {
                $("#more").show();
            }
        };
        this.successLoadingEvents = function (response) {
            var events = response.Events;
            var itemsProcessed = 0;
            events.forEach(function (event, index, array) {
                _this.itemContent(event);
                itemsProcessed++;
                if (itemsProcessed === array.length) {
                    $("time.timeago").timeago();
                }
            });
            _this.moreButton(response.TotalHits, response.ItemsPerPage);
            _this._pageIndex++;
            // Tags
            var tags = response.Tags;
            itemsProcessed = 0;
            tags.forEach(function (tg, index, array) {
                _this.tagItemContent(tg);
                itemsProcessed++;
                if (itemsProcessed === array.length) {
                    $("a[data-type='tag']").click(_this.filterTag);
                    _this.markTag();
                }
            });
        };
        this.emptyElements = function () {
            $("#eventcards").empty();
            $("#tags").empty();
        };
        this.search = function () {
            _this.emptyElements();
            _this._pageIndex = 0;
            _this.executeSearch();
        };
        this.markTag = function () {
            if (_this._tags.length > 0) {
                _this._tags.forEach(function (tg, index, array) {
                    var $callback_this = $("a[data-tag='" + tg + "']");
                    $callback_this.addClass("blue");
                    $callback_this.append("<i class=\"delete icon\"></i>");
                    $callback_this.unbind("click");
                });
            }
        };
        this.filterTag = function (e) {
            _this._lastTagObj = $(e.target);
            if (_this._lastTagObj.hasClass("detail")) {
                _this._lastTagObj = _this._lastTagObj.parent();
            }
            _this._tags.push(_this._lastTagObj.data("tag"));
            _this.search();
        };
        this.executeSearch = function () {
            var searchString = $("#search-input").val();
            var url = "api/events/filter?d=" + _this._daysInFuture + "&i=" + _this._pageIndex + "&t=" + _this._tags.join(",") + "&f=" + searchString;
            _this.getAjax(url, _this.successLoadingEvents);
        };
        this.removeTag = function (e) {
            var $tagToRemove = $(e.target);
            if ($tagToRemove.hasClass("delete")) {
                $tagToRemove = $tagToRemove.parent();
            }
            _this._tags = _this._tags.filter(function (el) {
                return el !== $tagToRemove.data("tag");
            });
            _this.search();
        };
        this.updateDays = function (e) {
            $("#tags").empty();
            var $day = $(e.target);
            _this._daysInFuture = $day.data("days");
            $("a[data-type='days']").removeClass("blue");
            $day.addClass("blue");
            _this.search();
        };
        this.more = function () {
            $("#tags").empty();
            _this.executeSearch();
        };
        this.detail = function (e) {
            var $card = $(e.currentTarget);
            var id = $card.data("id");
            console.log("modal: " + id);
            _this.getAjax("api/events/" + id, function (response) {
                _this.modalContent(response);
                $("time.timeago").timeago();
                $('.ui.modal').modal('setting', 'transition', 'fade up').modal('show');
            });
        };
        this.start = function () {
            $.timeago.settings.allowFuture = true;
            $("time.timeago").timeago();
            $("#more").click(_this.more);
            $("#search-btn").click(_this.search);
            $('#search-input').keypress(function (e) {
                if (e.which === 13) {
                    _this.search();
                }
            });
            $("a[data-type='tag']").click(_this.filterTag);
            $(document).on("click", "i[class='delete icon']", _this.removeTag);
            $("a[data-type='days']").click(_this.updateDays);
            $(document).on("click", "div[class='card']", _this.detail);
        };
        this.successImages = function (response) {
            var list = $('#img-list');
            $.each(response, function (key, value) {
                list.append($("<option></option>")
                    .attr("value", key)
                    .text(value));
            });
            list.change(function () {
                var file = $("#img-list option:selected").text();
                $('#img-preview').empty();
                $('#img-preview').prepend("<img src=\"http://res.cloudinary.com/forloop/image/upload/c_scale,h_250/sprofile/" + file + "\" />");
            });
        };
        this.getImages = function () {
            _this.getAjax("/api/events/images", _this.successImages);
        };
        this.createEvent = function () {
            var formData = $('form').serializeArray().reduce(function (obj, item) {
                obj[item.name] = item.value;
                return obj;
            }, {});
            var eventJson = {
                "apiKey": formData["api[key]"],
                "header": formData["event[header]"],
                "description": formData["event[description]"],
                "startDate": formData["event[startdate]"],
                "endDate": formData["event[enddate]"],
                "imageUrl": $("#img-list option:selected").text(),
                "tags": formData["event[tags]"].split(','),
                "location": {
                    "address": formData["location[address]"],
                    "city": formData["location[city]"],
                    "latitude": formData["location[latitude]"],
                    "longitude": formData["location[longitude]"],
                },
                "orginazer": {
                    "email": formData["orginazer[email]"],
                    "name": formData["orginazer[name]"],
                    "phone": formData["orginazer[phone]"],
                    "siteUrl": formData["orginazer[siteUrl]"],
                    "eventUrl": formData["orginazer[eventUrl]"],
                }
            };
            $.ajax({
                method: "POST",
                url: "/api/events",
                data: eventJson,
                dataType: "json",
            })
                .done(function (response) { return alert("Done! " + response); })
                .fail(function (callback) { return alert("Error: " + callback); });
        };
        this.create = function () {
            $("#create-btn").click(_this.createEvent);
            _this.getImages();
        };
    }
    return Sprofile;
}());
//# sourceMappingURL=app.js.map