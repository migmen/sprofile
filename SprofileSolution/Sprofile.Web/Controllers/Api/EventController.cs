﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Http;
using Sprofile.Web.Infrastructure;
using Sprofile.Web.Model;
using Newtonsoft.Json;
using System.Web;
using System.Threading.Tasks;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Sprofile.Web.Common;

namespace Sprofile.Web.Controllers.Api
{
    [RoutePrefix("api/events")]
    public class EventController : ApiController
    {
        private IActivityEventRepository _eventRepository;
        private ISprofileConfiguration _configuration;
        private ILogHelper _logHelper;


        public EventController(IActivityEventRepository eventRepository, ISprofileConfiguration configuration, ILogHelper logHelper)
        {
            _eventRepository = eventRepository;
            _configuration = configuration;
            _logHelper = logHelper;
        }

        // GET http://localhost:46209/api/events/filter?d=30&i=0&t=&f=
        [HttpGet]
        [Route("filter")]
        public IHttpActionResult GetByFilter([FromUri] int d, [FromUri] int i, [FromUri] string t = null, [FromUri] string f = null)
        {
            int daysInFuture = d;
            int index = i;
            string tag = t;
            string searchFilter = f;
            DateTime start = DateTime.Now.Date;
            DateTime end = start.AddDays(daysInFuture);

            var result = _eventRepository.GetEventsWithTags(start, end, tag, searchFilter, index, _configuration.ItemsPerPage);
            var tags = result?.aggregations?.by_tags?.buckets.Select(x => new Tag() { Key = x.key, Count = x.doc_count });

            var dateRangeTagResult = _eventRepository.GetEventTags(start, end, _configuration.ItemsPerPage);
            var dateRangeTags = dateRangeTagResult?.aggregations?.by_tags?.buckets.Select(x => new Tag() { Key = x.key, Count = x.doc_count }).ToList();

            dateRangeTags.ForEach((x) => x.Count = 0);
            foreach (var tg in tags)
            {
                var found = dateRangeTags.FirstOrDefault(x => x.Key == tg.Key);
                if (found != null)
                {
                    found.Count = tg.Count;
                }
            }

            var events = new List<ActivityEvent>(result.hits.hits.Select(h => h._source)).ToList();
            if (events.Any())
            {
                events.ForEach(e => e.Description = e.Description.Truncate(150));
            }

            return Ok(new ActivityEventResultModel()
            {
                TotalHits = result.hits.total,
                Events = events,
                ItemsPerPage = _configuration.ItemsPerPage,
                Tags = dateRangeTags,
                StartDate = start,
                EndDate = end
            });
        }

        // GET http://localhost:46209/api/events/21279949-6cbe-4e5e-815d-7b1e0be75117
        [HttpGet]
        [Route("{id}")]
        public IHttpActionResult GetById(Guid id)
        {
            var evt = _eventRepository.GetById(id.ToString());
            return Ok(evt);
        }

        // POST http://localhost:46209/api/events
        /*
        {
            ApiKey: "48D84213-32E3-4F03-9F71-32738705D0C4"
            Header: "Spela på heden",
            Description: "Vi tänkte köra en match på heden",
            StartDate: "2017-03-20 12:00:00",
            EndDate: "2017-03-20 15:00:00",
            ImageUrl: "soccer-263716.jpg",
            Location: 
            {
                Address: "Heden fotbollsplan",
                City: "Göteborg",
                Latitude: 57.70179461,
                Longitude: 11.97885704
            },
            Orginazer: 
            {
                Email: "test@test.se",
                EventUrl: "http://www.gp.se",
                Name: "Pelle Testsson",
                Phone: "0702123456"
            },
            Tags: ["Fotboll","Soccer"]
        }
        */
        [HttpPost]
        [Route("")]
        public IHttpActionResult AddEvent([FromBody] ActivityEventApiModel model)
        {
            // Simple security check
            if (model.ApiKey != Guid.Parse("48D84213-32E3-4F03-9F71-32738705D0C4")) return Unauthorized();

            var activityEvent = new ActivityEvent()
            {
                Header = model.Header,
                Description = model.Description,
                StartDate = model.StartDate,
                EndDate = model.EndDate,
                ImageUrl = model.ImageUrl,
                Location = model.Location != null ? new Location()
                {
                    Address = model.Location.Address,
                    City = model.Location.City,
                    Coordinates = new Model.Coordinates() { Lat = model.Location.Latitude, Lon = model.Location.Longitude }
                } : null,
                Orginazer = model.Orginazer != null ? new Organizer()
                {
                    Email = model.Orginazer.Email,
                    EventUrl = model.Orginazer.EventUrl,
                    Name = model.Orginazer.Name,
                    Phone = model.Orginazer.Phone,
                    SiteUrl = model.Orginazer.SiteUrl
                } : null,
                Tags = model.Tags != null ? model.Tags.Select(s => CultureInfo.CurrentCulture.TextInfo.ToTitleCase(s.Trim().ToLower()) ) : null
            };

            _eventRepository.AddOrUpdate(activityEvent);

            _logHelper.Info($"EventCreated. Id:{activityEvent.Id}, Header:{activityEvent.Header}, StartDate:{activityEvent.StartDate:d}, EndDate:{activityEvent.EndDate:d}.");

            SaveBackup(activityEvent, HttpContext.Current);

            return Ok(activityEvent.Id);
        }


        // DELETE http://localhost:46209/api/events/B4DF384B-E162-4F2D-AD28-48A49A008014
        [HttpDelete]
        [Route("{id}/{apiKey}")]
        public IHttpActionResult RemoveEvent(Guid id, Guid apiKey)
        {
            // Simple security check
            if (apiKey != Guid.Parse("48D84213-32E3-4F03-9F71-32738705D0C4")) return Unauthorized();

            _eventRepository.RemoveById(id.ToString());

            return Ok();
        }


        // GET http://localhost:46209/api/events/heartbeat
        [HttpGet]
        [Route("heartbeat")]
        public IHttpActionResult GetHeartBeat()
        {
            return Ok(_eventRepository != null);
        }


        // GET http://localhost:46209/api/events/images
        [HttpGet]
        [Route("images")]
        public IHttpActionResult GetImages()
        {
            // Url: https://github.com/cloudinary/CloudinaryDotNet/blob/master/Cloudinary.Test/CloudinaryTest.cs

            var account = new Account("forloop", "478984361342318", "2M1-KcEcwYviMEgSEj9tuAcqYuY");
            var cloudinary = new Cloudinary(account);

            var listParams = new ListResourcesParams()
            {
                ResourceType = ResourceType.Image,
                MaxResults = 100,
                Tags = true
            };

            var result = cloudinary.ListResources(listParams);
            
            List<string> list = result.Resources.ToList().Where(w => w.Tags.Contains("sprofile")).Select(r => r.SecureUri.ToString().Split('/').Last()).ToList();

            return Ok(list);
        }

        private async Task SaveBackup(ActivityEvent activityEvent, HttpContext context)
        {
            // I have a bunch of async work to do, and I am executed on the UI thread.
            await Task.Run(() => SaveToFile(activityEvent, context));
        }

        private void SaveToFile(ActivityEvent activityEvent, HttpContext context)
        {
            string jsonString = JsonConvert.SerializeObject(activityEvent);
            string filename = $"event_{activityEvent.Occured.ToString("yyyyMMdd.HHmmss")}.json";
            var path = context.Server.MapPath("~/backup");

            System.IO.File.WriteAllText($@"{path}\{filename}", jsonString);
        }
    }



    public class ActivityEventApiModel
    {
        public Guid ApiKey { get; set; }
        public string Header { get; set; }
        public string ImageUrl { get; set; }
        public LocationApiModel Location { get; set; } = new LocationApiModel();
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
        public OrganizerApiModel Orginazer { get; set; } = new OrganizerApiModel();
        public string Description { get; set; }
        public IEnumerable<string> Tags { get; set; }
    }

    public class LocationApiModel
    {
        public string City { get; set; }
        public string Address { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }

    public class OrganizerApiModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string SiteUrl { get; set; }
        public string EventUrl { get; set; }
    }
}
