﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Sprofile.Web.Common;
using Sprofile.Web.Infrastructure;
using Sprofile.Web.Model;

namespace Sprofile.Web.Controllers
{
    public class HomeController : Controller
    {
        private IActivityEventRepository _eventRepository;
        private ISprofileConfiguration _configuration;

        public HomeController(IActivityEventRepository eventRepository, ISprofileConfiguration configuration)
        {
            _eventRepository = eventRepository;
            _configuration = configuration;
        }

        public ActionResult Index()
        {
            DateTime today = DateTime.Now.Date;
            DateTime start = today;
            DateTime end = today.AddDays(_configuration.DaysInFuture);

            var result =_eventRepository.GetEventsWithTags(start, end, null, null, 0, _configuration.ItemsPerPage);
            var activityEventTags = _eventRepository.GetEventTags(start, end, _configuration.ItemsPerPage);

            var tags = activityEventTags?.aggregations?.by_tags?.buckets.Select(x => new Tag() {Key = x.key, Count = x.doc_count});
            var events = new List<ActivityEvent>(result.hits.hits.Select(h => h._source)).ToList();
            if (events.Any())
            {
                events.ForEach(e => e.Description = e.Description.Truncate(150));
            }

            var resultModel = new ActivityEventResultModel()
            {
                TotalHits = result.hits.total,
                Events = events,
                ItemsPerPage = _configuration.ItemsPerPage,
                Tags = tags,
                StartDate = start,
                EndDate = end
            };

            return View(resultModel);
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Error(string message)
        {
            ViewBag.Message = message;

            return View();
        }

        public ActionResult DesignTest()
        {
            return View();
        }
    }
}