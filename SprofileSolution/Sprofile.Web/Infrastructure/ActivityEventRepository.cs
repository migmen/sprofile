﻿using System;
using System.Collections.Generic;
using Elasticsearch.Net;
using Nest;
using Newtonsoft.Json;
using Sprofile.Web.Model;
using System.Diagnostics;

namespace Sprofile.Web.Infrastructure
{
    public interface IActivityEventRepository
    {
        void AddOrUpdate<T>(T model) where T : BaseModel;
        void InsertBulk<T>(List<T> models) where T : BaseModel;
        ActivityEvent GetById(string id);
        void RemoveById(string id);
        ActivityEventTagsGroup GetEventTags(DateTime startDate, DateTime endDate, int take);
        ActivityEventTagsGroup GetEventsWithTags(DateTime startDate, DateTime endDate, string tag, string stringfilter, int startIndex, int take);
    }


    public class ActivityEventRepository : IActivityEventRepository
    {
        private readonly IElasticsearch _elasticSearch;
        private readonly ILogHelper _logHelper;
        private const string _activityEventType = "activityevent";

        public ActivityEventRepository(IElasticsearch elasticSearch, ILogHelper logHelper)
        {
            _elasticSearch = elasticSearch;
            _logHelper = logHelper;
        }

        public void AddOrUpdate<T>(T model) where T : BaseModel
        {
            _elasticSearch.Index(model.Id, model);
        }

        public void InsertBulk<T>(List<T> models) where T : BaseModel
        {
            var operations = new List<IBulkOperation>();

            foreach (var model in models)
            {
                operations.Add(new BulkIndexOperation<T>(model));
            }

            var response = _elasticSearch.Bulk(operations);
        }

        public ActivityEvent GetById(string id)
        {
            return _elasticSearch.GetById<ActivityEvent>(id);
        }

        public void RemoveById(string id)
        {
            _elasticSearch.Remove<ActivityEvent>(id);
        }

        public ActivityEventTagsGroup GetEventsWithTags(
            DateTime startDate,
            DateTime endDate,
            string tag,
            string stringfilter,
            int startIndex,
            int take)
        {
            string matchesJson = string.Empty;

            if (!string.IsNullOrEmpty(stringfilter))
            {
                object multi_match = new
                {
                    query = stringfilter,
                    type = "best_fields",
                    // "tags^2" "location.city" "location.address"  , "email", "name"
                    fields = new[] { "header^3", "description^1", "location.city^2", "location.address" },
                    tie_breaker = 0.3,
                    @operator = "or"
                };

                matchesJson = $@"{{ ""multi_match"": {JsonConvert.SerializeObject(multi_match)} }}";
            }

            if (!string.IsNullOrEmpty(tag))
            {
                object match = new
                {
                    tags = new
                    {
                        query = tag,
                        @operator = "or"
                    }
                };

                if (!string.IsNullOrEmpty(matchesJson))
                {
                    matchesJson += ",";
                }
                matchesJson += $@"{{ ""match"": {JsonConvert.SerializeObject(match)} }}";
            }

            if (!string.IsNullOrEmpty(matchesJson))
            {
                matchesJson += ",";
            }

            matchesJson += $"{{ \"term\" : {{ \"_type\" : \"{_activityEventType}\" }}}}";


            var filter = new
            {
                range = new
                {
                    startDate = new
                    {
                        gte = startDate.ToString("yyyy-MM-dd HH:mm"),
                        lte = endDate.ToString("yyyy-MM-dd HH:mm"),
                        format = "yyyy-MM-dd HH:mm"
                    }
                }
            };

            var qJson = $@"
            {{
                ""from"": {startIndex * take},
                ""size"": {take},
                ""aggs"":
                {{
                    ""by_tags"":
                    {{
                        ""terms"":
                        {{
                            ""field"": ""tags.keyword"",
                            ""size"": 20
                        }}
                    }}
                }},
                ""query"":
                {{
                    ""bool"": 
                    {{
                        ""filter"": {JsonConvert.SerializeObject(filter)},
                        ""must"": [
                        {matchesJson}
                        ]
                    }}
                }},
               ""sort"": [
                  {{""startDate"": {{""order"": ""asc""}} }}
               ]
            }}";

            var client = _elasticSearch.Client();

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            ElasticsearchResponse<string> response = client.LowLevel.Search<string>(_elasticSearch.DefaultIndex, qJson);
            stopwatch.Stop();

            _logHelper.SearchLog(startDate, endDate, tag, stringfilter, startIndex, take, qJson, _elasticSearch.DefaultIndex, stopwatch);

            return JsonConvert.DeserializeObject<ActivityEventTagsGroup>(response.Body);
        }


        public ActivityEventTagsGroup GetEventTags(DateTime startDate, DateTime endDate, int take)
        {
            var requestBody = new
            {
                size = take,
                aggs = new
                {
                    by_tags = new
                    {
                        terms = new
                        {
                            field = "tags.keyword",
                            size = 20
                        }
                    }
                },
                query = new
                {
                    @bool = new
                    {
                        filter = new
                        {
                            range = new
                            {
                                startDate = new
                                {
                                    gte = startDate.ToString("yyyy-MM-dd HH:mm"),
                                    lte = endDate.ToString("yyyy-MM-dd HH:mm"),
                                    format = "yyyy-MM-dd HH:mm"
                                }
                            }
                        },
                        must = new
                        {
                            term = new
                            {
                                _type = _activityEventType
                            }
                        }
                    }
                }
            };
            string qJson = JsonConvert.SerializeObject(requestBody);

            var client = _elasticSearch.Client();
            ElasticsearchResponse<string> response = client.LowLevel.Search<string>(_elasticSearch.DefaultIndex, qJson);

            return JsonConvert.DeserializeObject<ActivityEventTagsGroup>(response.Body);
        }
    }
}
