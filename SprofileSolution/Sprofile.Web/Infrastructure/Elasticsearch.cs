﻿using System;
using System.Collections.Generic;
using Elasticsearch.Net;
using Nest;
using Sprofile.Web.Common;
using Sprofile.Web.Model;

namespace Sprofile.Web.Infrastructure
{
    public interface IElasticsearch
    {
        string DefaultIndex { get; }

        void Index<T>(Guid id, T model) where T : class;
        //ISearchResponse<T> Search<T>(Func<SearchDescriptor<T>, SearchDescriptor<T>> searchSelector) where T : class;
        T GetById<T>(string id) where T : class;
        IBulkResponse Bulk(List<IBulkOperation> operations);
        void Remove<T>(string id) where T : class;
        ElasticClient Client();
    }



    public class Elasticsearch : IElasticsearch
    {
        private readonly ISprofileConfiguration _configuration;

        private ConnectionSettings _connectionSettings;
        private ElasticClient _elasticClient;
        private StaticConnectionPool _connectionPool;

        public string DefaultIndex { get; } = "sprofile-events";


        public Elasticsearch(ISprofileConfiguration configuration)
        {
            _configuration = configuration;
        }


        public T GetById<T>(string id) where T : class
        {
            var model = GetClient().Get<T>(id);
            return model.Found ? model.Source : null;
        }

        public void Remove<T>(string id) where T : class
        {
            GetClient().Delete<T>(id);
        }

        public void Index<T>(Guid id, T model) where T : class
        {
            GetClient().Index(model, i => i.Id(id));
        }

        public IBulkResponse Bulk(List<IBulkOperation> operations)
        {
            var request = new BulkRequest()
            {
                Refresh = Refresh.True,
                //Consistency = Consistency.One,
                Operations = operations
            };

            IBulkResponse response = GetClient().Bulk(request);
            return response;
        }

        //public ISearchResponse<T> Search<T>(Func<SearchDescriptor<T>, SearchDescriptor<T>> searchSelector) where T : class
        //{
        //    return GetClient().Search(searchSelector);
        //}

        public ElasticClient Client()
        {
            return GetClient();
        }

        private ElasticClient GetClient()
        {
            if (_elasticClient == null)
            {
                var nodes = new Uri[]
                {
                    new Uri(_configuration.ElasticsearchUrl),
                    //new Uri("Add server 2 address")   //Add cluster addresses here
                    //new Uri("Add server 3 address")
                };

                _connectionPool = new StaticConnectionPool(nodes);
                _connectionSettings = new ConnectionSettings(_connectionPool)
                    .DefaultIndex(DefaultIndex);

                _elasticClient = new ElasticClient(_connectionSettings);
            }
            return _elasticClient;
        }
    }
}
