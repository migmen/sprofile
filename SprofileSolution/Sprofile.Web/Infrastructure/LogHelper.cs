﻿using Newtonsoft.Json;
using Sprofile.Web.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Web;
using Sprofile.Web.Common;

namespace Sprofile.Web.Infrastructure
{
    public interface ILogHelper
    {
        void Debug(string message, [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string memberName = "");
        void Error(string message, Exception exception = null, [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string memberName = "");
        void Info(string message, [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string memberName = "");
        void Trace(string message, [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string memberName = "");
        void Warn(string message, [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string memberName = "");
        void SearchLog(DateTime startDate, DateTime endDate, string tags, string searchString, int startIndex, int take, string query, string index, Stopwatch stopWatch);
        void PerformanceMessage(MessageType type, string correlationId, HttpRequestMessage request, byte[] message, Stopwatch stopWatch = null);
    }

    public class LogHelper : ILogHelper
    {
        private ILogRepository _logRepository;

        public LogHelper(ILogRepository logRepository)
        {
            _logRepository = logRepository;
        }

        public void Trace(string message, [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string memberName = "")
        {
            _logRepository.AddOrUpdate(new Model.LogEvent() { Message = message, LogType = LogType.Trace.ToString(), FilePath = filePath, LineNumber = lineNumber, MemberName = memberName });
            Logging.Trace(Logging.BuildMessage(message, LogType.Trace, filePath, lineNumber, memberName));
        }

        public void Debug(string message, [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string memberName = "")
        {
            _logRepository.AddOrUpdate(new Model.LogEvent() { Message = message, LogType = LogType.Debug.ToString(), FilePath = filePath, LineNumber = lineNumber, MemberName = memberName });
            Logging.Debug(Logging.BuildMessage(message, LogType.Debug, filePath, lineNumber, memberName));
        }

        public void Info(string message, [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string memberName = "")
        {
            _logRepository.AddOrUpdate(new Model.LogEvent() { Message = message, LogType = LogType.Info.ToString(), FilePath = filePath, LineNumber = lineNumber, MemberName = memberName });
            Logging.Info(Logging.BuildMessage(message, LogType.Info, filePath, lineNumber, memberName));
        }

        public void Warn(string message, [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string memberName = "")
        {
            _logRepository.AddOrUpdate(new Model.LogEvent() { Message = message, LogType = LogType.Warning.ToString(), FilePath = filePath, LineNumber = lineNumber, MemberName = memberName });
            Logging.Warn(Logging.BuildMessage(message, LogType.Warning, filePath, lineNumber, memberName));
        }

        public void Error(string message, Exception exception = null, [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string memberName = "")
        {
            _logRepository.AddOrUpdate(new Model.LogEvent() { Message = message, LogType = LogType.Error.ToString(), FilePath = filePath, LineNumber = lineNumber, MemberName = memberName, Exception = exception?.ToString() });
            Logging.Error(Logging.BuildMessage(message, LogType.Error, filePath, lineNumber, memberName), exception);
        }

        public void SearchLog(DateTime startDate, DateTime endDate, string tags, string searchString, int startIndex, int take, string query, string index, Stopwatch stopWatch)
        {
            var log = new SearchLog()
            {
                StartDate = startDate,
                EndDate = endDate,
                SearchString = searchString,
                Tags = tags,
                StartIndex = startIndex,
                Take = take,
                Query = query,
                Index = index,
                ElapsedMilliseconds = stopWatch?.Elapsed.TotalMilliseconds,
                ElapsedSeconds = (stopWatch?.Elapsed.TotalMilliseconds / 1000),
            };

            _logRepository.AddOrUpdate(log);

            stopWatch.Reset();
        }

        public void PerformanceMessage(MessageType type, string correlationId, HttpRequestMessage request, byte[] message, Stopwatch stopWatch = null)
        {
            var threadId = Thread.CurrentThread?.ManagedThreadId ?? -1;
            var activityId = System.Diagnostics.Trace.CorrelationManager?.ActivityId.ToString() ?? string.Empty;
            var msg = (message != null) ? Encoding.UTF8.GetString(message) : string.Empty;

            var context = ((HttpContextBase)request.Properties["MS_HttpContext"]);
            var contextRequest = context?.Request;
            var routeData = request.GetRouteData();
            var headers = (request.Headers != null) ? SerializeHeaders(request.Headers) : string.Empty;

            /*
             * apiLogEntry.ResponseStatusCode = (int)response.StatusCode;
             * User = context.User.Identity.Name,
             * Machine = Environment.MachineName,
            */

            var log = new PerformanceMetrix()
            {
                MessageType = type.ToString(),
                Method = request.Method.ToString(),
                AbsolutePath = request.RequestUri.AbsolutePath,
                DomainHost = request.RequestUri.Host,
                RequestUri = request.RequestUri.ToString(),
                PathAndQuery = request.RequestUri.PathAndQuery,
                Message = msg,
                ElapsedMilliseconds = stopWatch?.Elapsed.TotalMilliseconds,
                ElapsedSeconds = (stopWatch?.Elapsed.TotalMilliseconds / 1000),
                CorrelationId = correlationId,
                ActivityId = activityId,
                ThreadId = threadId,
                Bytes = message?.Length ?? 0,
                ContentType = contextRequest?.ContentType,
                IpAddress = contextRequest?.UserHostAddress,
                RouteTemplate = routeData.Route.RouteTemplate,
                Headers = headers
            };

            _logRepository.AddOrUpdate(log);
            //Logging.Trace(Logging.BuildMessage(message, LogType.Trace, filePath, lineNumber, memberName));

            if (stopWatch != null) stopWatch.Reset();
        }


        private string SerializeHeaders(HttpHeaders headers)
        {
            var dict = new Dictionary<string, string>();

            foreach (var item in headers.ToList())
            {
                if (item.Value != null)
                {
                    var header = String.Empty;
                    foreach (var value in item.Value)
                    {
                        header += value + " ";
                    }

                    // Trim the trailing space and add item to the dictionary
                    header = header.TrimEnd(" ".ToCharArray());
                    dict.Add(item.Key, header);
                }
            }

            return JsonConvert.SerializeObject(dict, Formatting.Indented);
        }
    }
}