﻿using Sprofile.Web.Model;

namespace Sprofile.Web.Infrastructure
{
    public interface ILogRepository
    {
        void AddOrUpdate<T>(T model) where T : BaseModel;
    }

    public class LogRepository : ILogRepository
    {
        private readonly IElasticsearch _elasticSearch;

        public LogRepository(IElasticsearch elasticSearch)
        {
            _elasticSearch = elasticSearch;
        }

        public void AddOrUpdate<T>(T model) where T : BaseModel
        {
            _elasticSearch.Index(model.Id, model);
        }
    }
}