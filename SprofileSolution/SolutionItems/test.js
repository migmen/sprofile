/// <reference path="../node_modules/@types/jquery/index.d.ts" />
/// <reference path="../node_modules/@types/jquery.timeago/index.d.ts" />
//interface ISprofileConfig {
//    pageIndex: number,
//    rootUrl: string,
//}
//declare var config: ISprofileConfig;
var Sprofile = (function () {
    //config: sprofileConfig;
    function Sprofile() {
        this._template = "<div class=\"card\">\
            <div class=\"image\">\
                <img src=\"{imgsrc}?height=250\">\
            </div>\
            <div class=\"content\">\
                <div class=\"header\">{header}</div>\
                <div class=\"meta\">\
                    <a class=\"ui small label\"><i class=\"calendar icon\"></i><time class=\"timeago\" datetime=\"{startdatetime}\" title=\"{startdate}\">-</time></a>\
                    <div class=\"ui small label\"><i class=\"wait icon\"></i> {starttime} - {endtime}</div>\
                </div>\
                <div class=\"description\">\
                    <p>{description}</p>\
                    <p class=\"mini\">Arrangör: {name} ({email})</p>\
                </div>\
            </div>\
            <div class=\"extra content\">\
                <i class=\"marker icon\"></i>{city}, {address}\
            </div>\
            </div>";
        this._configPageIndex = 1; // : number
        this._configPageIndex = 1;
    }
    // getAjax(url: string, success: (response: any) => any)
    Sprofile.prototype.getAjax = function (url) {
        console.log("getAjax");
        return $.ajax({
            method: "GET",
            url: url,
            dataType: "json",
        });
        //.done((response) => {
        //    success(response);
        //});
    };
    // public myMethod = (useBigString:boolean) : string => {
    //test = (a: any, b: any) : void => {
    //    console.log('a: ' + a);
    //    console.log('b: ' + b);
    //}
    //test: function(a: any, b: any) {
    //    console.log('a: ' + a);
    //    console.log('b: ' + b);
    //};
    //test(a: any, b: any) {
    //    console.log('a: ' + a);
    //    console.log('b: ' + b);
    //}
    Sprofile.prototype.formatDate = function (d) {
        var month = d.getMonth() + 1;
        var day = d.getDate();
        var output = d.getFullYear() + '-' +
            (('' + month).length < 2 ? '0' : '') + month + '-' +
            (('' + day).length < 2 ? '0' : '') + day;
        return output;
    };
    Sprofile.prototype.getDate = function () {
        return this.formatDate(new Date());
    };
    Sprofile.prototype.addZero = function (num) {
        if (String(num).length === 1) {
            return "0" + num;
        }
        return num;
    };
    Sprofile.prototype.m = function (month) {
        return month + 1;
    };
    Sprofile.prototype.itemContent = function (evt) {
        var sd = new Date(evt.StartDate);
        var ed = new Date(evt.EndDate);
        var event = this._template
            .replace("{imgsrc}", evt.ImageUrl)
            .replace("{header}", evt.Header)
            .replace("{startdatetime}", sd.getFullYear() + "-" + this.m(sd.getMonth()) + "-" + sd.getDate() + " " + this.addZero(sd.getHours()) + ":" + this.addZero(sd.getMinutes()))
            .replace("{startdate}", sd.getFullYear() + "-" + this.m(sd.getMonth()) + "-" + sd.getDate())
            .replace("{starttime}", this.addZero(sd.getHours()) + ":" + this.addZero(sd.getMinutes()))
            .replace("{endtime}", this.addZero(ed.getHours()) + ":" + this.addZero(ed.getMinutes()))
            .replace("{description}", evt.Description)
            .replace("{name}", evt.Orginazer.Name)
            .replace("{email}", evt.Orginazer.Email)
            .replace("{city}", evt.Location.City)
            .replace("{address}", evt.Location.Address);
        $("#eventcards").append(event);
    };
    Sprofile.prototype.moreButton = function (total, perPage) {
        var loaded = (this._configPageIndex + 1) * perPage;
        if (total <= loaded) {
            $("#more").remove();
        }
    };
    Sprofile.prototype.successLoadingEvents = function (response) {
        var _this = this;
        console.log("successLoadingEvents");
        var events = response.Events;
        var itemsProcessed = 0;
        events.forEach(function (event, index, array) {
            _this.itemContent(event);
            itemsProcessed++;
            if (itemsProcessed === array.length) {
                $("time.timeago").timeago();
            }
        });
        this.moreButton(parseInt(response.TotalHits), response.ItemsPerPage);
        this._configPageIndex++;
    };
    Sprofile.prototype.loadEvents = function () {
        var url = "api/events?startIndex=" + this._configPageIndex;
        console.log(url);
        $.ajax({
            method: "GET",
            url: url,
            dataType: "json",
        })
            .done(this.successLoadingEvents);
        //this.getAjax(url).done(this.successLoadingEvents);
    };
    Sprofile.prototype.mendes = function (a) {
        console.log('a: ' + a);
    };
    Sprofile.prototype.search = function () {
        $("#eventcards").empty();
        var searchString = $("#search-input").val();
        //var startDate = this.formatDate($("#rangestart").calendar("get date"));
        //var endDate = this.formatDate($("#rangeend").calendar("get date"));
        this._configPageIndex = 0;
        //this.mendes('testa', 'testb');
        //var url = this.config.rootUrl + "api/events/filter/" + searchString + "/?start=" + startDate + "&end=" + endDate + "&i=" + this.config.pageIndex;
        var url = "api/events/filter/" + searchString + "/?start=2017-02-10&end=2017-03-10&i=" + this._configPageIndex;
        console.log(url);
        //this.test(url, "test"); // 
        //this.test(url, this.successLoadingEvents); // 
        console.log("search 2");
    };
    Sprofile.prototype.thevoid = function (url, runme) {
        runme(url);
    };
    Sprofile.prototype.start = function () {
        this._configPageIndex = 1;
        $.timeago.settings.allowFuture = true;
        $("time.timeago").timeago();
        $("#more").click(this.loadEvents);
        $("#search-btn").click(this.search);
        //this.thevoid('url.mendes', this.mendes);
        //$("#rangestart").calendar({
        //    type: "date",
        //    firstDayOfWeek: 1,
        //    today: true,
        //    minDate: new Date(),
        //    endCalendar: $("#rangeend")
        //});
        //$("#rangeend").calendar({
        //    type: "date",
        //    firstDayOfWeek: 1,
        //    startCalendar: $("#rangestart")
        //});
    };
    return Sprofile;
}());
$(document).ready(function () {
    var sprofile = new Sprofile();
    sprofile.start();
});
//# sourceMappingURL=app.js.map